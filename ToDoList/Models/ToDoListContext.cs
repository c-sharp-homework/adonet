using Microsoft.EntityFrameworkCore;

namespace ToDoList.Models;

public class ToDoListContext(DbContextOptions options) : DbContext(options)
{
    public DbSet<Item> Items { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Status> Statuses { get; set; }
}