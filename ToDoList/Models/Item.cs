namespace ToDoList.Models;

public class Item
{
    public int ItemId { get; set; }
    public string Description { get; set; }
    public int CategoryId { get; set; }
    public int StatusId { get; set; }
    public string Name { get; set; }
    
    public Category Category { get; set; }
    public Status Status { get; set; }
}