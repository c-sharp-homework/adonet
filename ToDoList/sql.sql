DROP TABLE IF EXISTS `categories`;
create table `categories`
(
    `CategoryId` int not null auto_increment,
    `Name` varchar(45) default null,
    primary key (`CategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Statuses`;
create table `Statuses`
(
    `StatusId` int not null auto_increment,
    `Name` varchar(30),
    primary key (`StatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists `items`;
create table `items`
(
    `ItemId` int not null auto_increment,
    `Name` varchar(50) default null,
    `Description` varchar(200) default null,
    `CategoryId` int default null,
    `DateTime` DATETIME default now(),
    `StatusId` int default 0,
    primary key (`ItemId`),
    key `categoryid_idx` (`CategoryId`),
    key `statusid_idx` (`StatusId`),
    constraint `categoryid` foreign key (`CategoryId`) references `categories` (`CategoryId`) on delete cascade on update cascade,
    constraint `statusid` foreign key (`StatusId`) references `Statuses` (`StatusId`) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;