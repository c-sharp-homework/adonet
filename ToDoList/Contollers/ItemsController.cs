using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Models;

namespace ToDoList.Contollers;

// var tasksWithCategories = _context.Tasks.Include(t => t.Category).ToList();
// var task = _context.Tasks.Find(taskId);
// _context.Entry(task).Reference(t => t.Category).Load();
// var category = _context.Categories.Find(categoryId);
// _context.Entry(category).Collection(c => c.Tasks).Load();

public class ItemsController : Controller
{
    private readonly ToDoListContext _db;

    public ItemsController(ToDoListContext db)
    {
        _db = db;
    }

    public ActionResult Index()
    {
        var model = _db.Items
            .Include(item => item.Category)
            .Include(item => item.Status)
            .ToList();
        return View(model);
    }

    public ActionResult Create()
    {
        ViewBag.CategoryId = new SelectList(_db.Categories, "CategoryId", "Name");
        ViewBag.StatusId = new SelectList(_db.Statuses, "StatusId", "Name");
        return View();
    }

    [HttpPost]
    public ActionResult Create(Item item)
    {
        if (item.CategoryId == 0)
        {
            return RedirectToAction("Create");
        }
        _db.Items.Add(item);
        _db.SaveChanges();
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        var thisItem = _db.Items
            .Include(item => item.Category)
            .Include(item => item.Status)
            .FirstOrDefault(item => item.ItemId == id);
        return View(thisItem);
    }

    public ActionResult Edit(int id)
    {
        var thisItem = _db.Items.FirstOrDefault(item => item.ItemId == id);
        ViewBag.CategoryId = new SelectList(_db.Categories, "CategoryId", "Name");
        ViewBag.StatusId = new SelectList(_db.Statuses, "StatusId", "Name");
        return View(thisItem);
    }

    [HttpPost]
    public ActionResult Edit(Item item)
    {
        _db.Items.Update(item);
        _db.SaveChanges();
        return RedirectToAction("Index");
    }

    public ActionResult Delete(int id)
    {
        var thisItem = _db.Items.FirstOrDefault(item => item.ItemId == id);
        return View(thisItem);
    }

    [HttpPost, ActionName("Delete")]
    public ActionResult DeleteConfirmed(int id)
    {
        var thisItem = _db.Items.FirstOrDefault(item => item.ItemId == id);
        _db.Items.Remove(thisItem);
        _db.SaveChanges();
        return RedirectToAction("Index");
    }
}