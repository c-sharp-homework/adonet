using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoList.Models;

namespace ToDoList.Contollers;

public class StatusController : Controller
{
    private readonly ToDoListContext context;

    public StatusController(ToDoListContext context)
    {
        this.context = context;
    }

    public ActionResult Index()
    {
        var model = context.Statuses.ToList();
        return View(model);
    }
    
    public ActionResult Create()
    {
        return View();
    }
    
    [HttpPost]
    public ActionResult Create(Status status)
    {
        context.Statuses.Add(status);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    public ActionResult Details(int id)
    {
        var status = context.Statuses.FirstOrDefault(status => status.StatusId == id);
        return View(status);
    }
    
    public ActionResult Edit(int id)
    {
        var thisItem = context.Items.FirstOrDefault(item => item.ItemId == id);
        return View(thisItem);
    }
    
    [HttpPost]
    public ActionResult Edit(Status status)
    {
        context.Statuses.Update(status);
        context.SaveChanges();
        return View();
    }
    
    public ActionResult Delete(int id)
    {
        var thisItem = context.Statuses.FirstOrDefault(item => item.StatusId == id);
        return View(thisItem);
    }
    
    [HttpPost, ActionName("Delete")]
    public ActionResult DeleteConfirmed(int id)
    {
        var thisItem = context.Statuses.FirstOrDefault(item => item.StatusId == id);
        context.Statuses.Remove(thisItem);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}