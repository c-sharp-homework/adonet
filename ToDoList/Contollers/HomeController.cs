using Microsoft.AspNetCore.Mvc;

namespace ToDoList.Contollers;

public class HomeController : Controller
{
    [HttpGet("/")]
    public ActionResult Index()
    {
        return View();
    }
}