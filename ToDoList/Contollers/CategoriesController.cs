using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoList.Models;

namespace ToDoList.Contollers;

public class CategoriesController : Controller
{
    private readonly ToDoListContext context;

    public CategoriesController(ToDoListContext context)
    {
        this.context = context;
    }
    
    public ActionResult Index()
    {
        var model = context.Categories.ToList();
        return View(model);
    }
    
    public ActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Create(Category category)
    {
        context.Categories.Add(category);
        context.SaveChanges();
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {
        var thisCat = context.Categories
            .Include(cat => cat.Items)
            .FirstOrDefault(cat => cat.CategoryId == id);
        return View(thisCat);
    }

    public ActionResult Edit(int id)
    {
        var thisCat = context.Categories.FirstOrDefault(cat => cat.CategoryId == id);
        return View(thisCat);
    }

    [HttpPost]
    public ActionResult Edit(Category category)
    {
        context.Categories.Update(category);
        context.SaveChanges();
        return RedirectToAction("Index");
    }

    public ActionResult Delete(int id)
    {
        var thisCat = context.Categories.FirstOrDefault(cat => cat.CategoryId == id);
        return View(thisCat);
    }
    
    [HttpPost, ActionName("Delete")]
    public ActionResult DeleteConfirmed(int id)
    {
        var thisCat = context.Categories.FirstOrDefault(category => category.CategoryId == id);
        context.Categories.Remove(thisCat);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}